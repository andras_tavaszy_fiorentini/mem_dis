﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Text.RegularExpressions;
using Microsoft.Win32;

namespace MemDis
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        const int width = 256;
        const int height = 1024;
        WriteableBitmap wbmap = new WriteableBitmap(width, height, 96, 96, PixelFormats.Bgra32, null);
        byte[] pixels = new byte[width*height*4];
        Int32Rect rect = new Int32Rect(0, 0, width, height);
        int stride = 4 * width;
        public MainWindow()
        {
            InitializeComponent();

            Label startAddressLabel = new Label();
            startAddressLabel.Content = "0x0000 -->";
            Canvas.SetLeft(startAddressLabel, 30);
            Canvas.SetTop(startAddressLabel, 20);
            Canvas1.Children.Add(startAddressLabel);
            Label endAddressLabel = new Label();
            endAddressLabel.Content = "0x3FFF -->";
            Canvas.SetLeft(endAddressLabel, 30);
            Canvas.SetTop(endAddressLabel, 1040);
            Canvas1.Children.Add(endAddressLabel);
            
            int coord;
            //fill byte array with black values
            for(int y= 0; y < height; y++)
            {
                for (int x=0; x<width*4; x+=4)
                {
                    
                    coord = y * width * 4 + x;
                    pixels[coord] = 0;
                    pixels[coord + 1] = 0;
                    pixels[coord + 2] = 0;
                    pixels[coord + 3] = (byte)(((y%2==0)/*||((x/4)%8 == 0)*/) ? 128 : 255);
                }
            }
            //ReadI32HexFile(@"C:\Work\DigitalCAP\Firmware\EWARM\TDD\Exe\DigitalCap-DevBoard.hex");
            //ReadI32HexFile(@"C:\Work\DigitalCAP\Firmware\EWARM\DigitalCap-DevBoard\Exe\DigitalCap-DevBoard.hex");
            wbmap.WritePixels(rect, pixels, stride, 0);
            Image1.Stretch = Stretch.None;
            Image1.Margin = new Thickness(0);
            Image1.Source = wbmap;
        }
        
        private void ReadI32HexFile(String filename)
        {
            
            try
            {
                uint baseAddress = 0x0;
                uint biggestAddress = 0;

                using (StreamReader sr = new StreamReader(filename))
                {
                    String line;
                    while (( line = sr.ReadLine())!=null)
                    {
                    Regex regex = new Regex(@":([0-9A-F]{2})([0-9A-Z]{4})([0-9A-F]{2})([0-9A-Z]*)([0-9A-Z]{2}\b)");
                    Match match = regex.Match(line);
                    // Record type
                    // 00 - Data record
                    // 01 - EoF
                    // 02 - Extended Segment Address
                    // 03 - Start Segment Address
                    // 04 - Extended Linear Address
                    // 05 - Start Linear Address
                    switch(match.Groups[3].Value)
                    {
                        case "00":
                            {
                                uint DataLength = uint.Parse(match.Groups[1].Value, System.Globalization.NumberStyles.HexNumber);
                                uint address = (baseAddress << 16) + uint.Parse(match.Groups[2].Value, System.Globalization.NumberStyles.HexNumber);
                                biggestAddress = address+DataLength > biggestAddress ? address+DataLength-1 : biggestAddress;
                                for (uint i = address; i< address+DataLength; i++)
                                {
                                    pixels[i*4 + 2] = 255;
                                }
                            }
                            break;
                        case "01":
                            {
                                // do nothing, end of file
                            }
                            break;
                            // Extended segment address, 16 bit segment base address - byte count is 02
                        case "02":
                            {
                                // do nothing, not an option in 32 bit addressing
                                throw new NotImplementedException();
                            }
                            //break;
                        case "03":
                            {

                            }
                            break;
                            // Extended linear address, 32 bit addressing - byte count is 02
                        case "04":
                            {
                                baseAddress = uint.Parse(match.Groups[4].Value, System.Globalization.NumberStyles.HexNumber) - 0x0800;
                            }
                            break;
                        case "05":
                            {

                            }
                            break;
                    }
                    }
                    Label biggestAddressLabel = new Label();
                    biggestAddressLabel.Content = "0x" + biggestAddress.ToString("X") + " -->";
                    Canvas.SetLeft(biggestAddressLabel, 30);
                    Canvas.SetTop(biggestAddressLabel, 20 + biggestAddress/256);
                    Canvas1.Children.Add(biggestAddressLabel);
                }
            }
            catch (IOException e)
            {
                Console.WriteLine("The file could not be read:\r\n{0}", e.Message);
            }
        }

        private void Button_Click(object sender, MouseButtonEventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            if(openFileDialog.ShowDialog() == true)
            {
                ReadI32HexFile(@"C:\Work\DigitalCAP\Firmware\EWARM\DigitalCap-DevBoard\Exe\DigitalCap-DevBoard.hex");
                wbmap.WritePixels(rect, pixels, stride, 0);
                Image1.Stretch = Stretch.None;
                Image1.Margin = new Thickness(0);
                Image1.Source = wbmap;
            }
        }
    }
}
